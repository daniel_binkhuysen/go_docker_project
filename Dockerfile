FROM quay.io/prometheus/busybox:latest
MAINTAINER Daniel Binkhuysen

COPY go_docker_project /bin/go_docker_project

EXPOSE 9222
ENTRYPOINT ["/bin/go_docker_project"]
