GO ?= go
GOFMT ?= $(GO)fmt

BIN_NAME ?= go_docker_project
BIN_DIR ?= $(shell pwd)

DOCKER_REPO ?= repo
DOCKER_IMAGE_NAME ?= go_docker_project
DOCKER_IMAGE_TAG ?= $(subst /,-,$(shell git rev-parse --abbrev-ref HEAD))

.PHONY: style
style:
	@echo ">> checking code style"
	@! $(GOFMT) -d $$(find . -name '*.go') | grep '^'
	@echo ">> done checking code style"

.PHONY: build
build:
	@echo ">> building binaries"
	$(GO) build -o $(BIN_NAME)

.PHONY: test
test:
	@echo ">> testing code"
	$(GO) test

.PHONY: tarball
tarball:
	@echo ">> building release tarball"
	$(PROMU) tarball --prefix $(BIN_NAME) $(BIN_DIR)

.PHONY: docker
docker:
	docker build -t "$(DOCKER_REPO)/$(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)" .

.PHONY: docker-publish
docker-publish:
	docker push "$(DOCKER_REPO)/$(DOCKER_IMAGE_NAME)"

.PHONY: release
release: build tarball docker docker-publish
